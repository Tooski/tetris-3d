/*
 * Josef Nosov
 * 
 * TCSS 305 - Autumn 2012
 * Tetris Project - Part 4
 * December 06, 2012
 */
package gui;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import java.util.concurrent.Callable;
import main.Main;

/**
 * Prevents from node thread concurrency.
 * @author Josef Nosov
 * @version 12/06/2012
 *
 */
public class CallableNodes {

  /**
   * Main holds the callable function.
   */
  private final Main my_main;
  
  /**
   * The node that is being added.
   */
  private final Node my_node;

  /**
   * Prevents threads from locking or overriding.
   * @param the_main the main frame.
   * @param the_node the node in question.
   */
  public CallableNodes(final Main the_main, final Node the_node) {
    my_main = the_main;
    my_node = the_node;
  }

  /**
   * @param the_object is the object being added to the node thread.
   */
  public void attachNode(final Object the_object) {
    my_main.enqueue(new Callable<Object>() {
      public Object call() {
        return my_node.attachChild((Spatial) the_object);
      }
    });
  }

  /**
   * @param the_object is the object being removed from the node thread.
   */
  public void detachNode(final Object the_object) {
    my_main.enqueue(new Callable<Object>() {
      public Object call() {
        return my_node.detachChild((Spatial) the_object);
      }
    });
  }

}
