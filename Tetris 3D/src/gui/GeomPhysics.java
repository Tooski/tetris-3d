/*
 * Josef Nosov
 * 
 * TCSS 305 - Autumn 2012
 * Tetris Project - Part 4
 * December 06, 2012
 */
package gui;

import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.scene.Geometry;

/**
 * Saves the physics of the geometric shape.
 * @author Josef Nosov
 * @version 12/06/2012
 *
 */
public class GeomPhysics {

  /**
   * Saves the body control.
   */
  private final RigidBodyControl my_control;
  
  /**
   * Saves the geometry.
   */
  private final Geometry my_geom;

  /**
   * 
   * @param the_control the physics shape of the object.
   * @param the_geometry the geometry of the object.
   */
  public GeomPhysics(final RigidBodyControl the_control, final Geometry the_geometry) {
    my_geom = the_geometry;
    my_control = the_control;
  }

  /**
   * @return returns the physics object.
   */
  public RigidBodyControl getRigidBodyControl() {
    return my_control;
  }

  /**
   * @return returns the geometry of the object.
   */
  public Geometry getGeometry() {
    return my_geom;
  }
}
