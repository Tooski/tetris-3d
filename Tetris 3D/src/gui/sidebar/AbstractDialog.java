/*
 * Josef Nosov
 * 
 * TCSS 305 - Autumn 2012 Tetris Project - Part 4 December 06, 2012
 */

package gui.sidebar;

import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;

import main.Main;

/**
 * Creates a dialog for the pop-up controller.
 * 
 * @author Josef Nosov
 * @version 12/06/2012
 * 
 */
@SuppressWarnings("serial")
public abstract class AbstractDialog extends JDialog {

  /**
   * Scales the frame.
   */
  protected static final double DENOMINATOR_SCALE = 3;
  
  /**
   * Scales the frame.
   */
  private static final double NUMERATOR_SCALE = 4;
  // changed to 4.

  /**
   * Allows child class to access main.
   */
  protected Main my_main;

  /**
   * The scale of the image.
   */
  protected double my_scale;
  /**
   * The background.
   */
  protected JLabel my_label;

  /**
   * Creates a dialog.
   * 
   * @param the_main the main frame.
   * @param the_image_filepath the image sets a background.
   */
  public AbstractDialog(final Main the_main, final String the_image_filepath) {
    super();
    my_main = the_main;
    final ImageIcon icon = createImage(the_image_filepath);
    my_scale = NUMERATOR_SCALE / DENOMINATOR_SCALE * 
        icon.getIconHeight() / the_main.getScreenHeight();
    my_label =
        new JLabel(new ImageIcon(icon.getImage().getScaledInstance((int) 
                                      (icon.getIconWidth() / my_scale),
                               (int) (icon.getIconHeight() / my_scale), Image.SCALE_SMOOTH)));

    setContentPane(my_label);
    createButton();
    resizeFrame();
  }

  /**
   * Resizes the frame.
   */
  private void resizeFrame() {
    pack();
    final int x_position =
        (int) ((my_main.getFrame().getLocationOnScreen().getX() + 
            my_main.getFrame().getWidth() / 2) - getSize().getWidth() / 2);
    final int y_position =
        (int) ((my_main.getFrame().getLocationOnScreen().getY() + 
            my_main.getFrame().getHeight() / 2) - getSize().getHeight() / 2);

    setResizable(false);
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    setLocation(x_position, y_position);
    setVisible(true);
    setAlwaysOnTop(true);
  }

  /**
   * Creates buttons for the popup window.
   */
  protected abstract void createButton();

  /**
   * Creates the image passed.
   * 
   * @param the_filepath the location of the file.
   * @return creates an image.
   */
  protected final ImageIcon createImage(final String the_filepath) {
    // Allows importation of images from the assets folder.
    return new ImageIcon(getClass().getResource(the_filepath));
  }
}
