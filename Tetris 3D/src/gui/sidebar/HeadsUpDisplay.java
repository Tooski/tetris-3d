/*
 * Josef Nosov
 * 
 * TCSS 305 - Autumn 2012
 * Tetris Project - Part 4
 * December 06, 2012
 */
package gui.sidebar;
import com.jme3.app.state.AbstractAppState;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.TextRenderer;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;
import gui.sidebar.sidetext.GameTimer;
import gui.sidebar.sidetext.Level;
import gui.sidebar.sidetext.LinesCleared;
import gui.sidebar.sidetext.ScoreTracker;
import javax.swing.JDialog;
import main.GameState;
import main.Main;


/**
 * Creates a heads up display.
 * @author Josef Nosov
 * @version 12/06/2012
 *
 */
public class HeadsUpDisplay extends AbstractAppState implements ScreenController {

  /**
   * Multiplies the score by 10.
   */
  private static final int MULTIPLIER = 10;

  /**
   * Sets the new heads up display screen.
   */
  private static final String HUD = "hud";

  /**
   * The nifty GUI creator.
   */
  private Nifty my_nifty;

  /**
   * The nifty's screen.
   */
  private Screen my_screen;
  /**
   * The game timer.
   */
  private final GameTimer my_timer;

  /**
   * The main frame.
   */
  private final Main my_main;
  
  /**
   * The score tracker.
   */
  private final ScoreTracker my_score;
  
  /**
   * The current level.
   */
  private final Level my_level;
  
  /**
   * The lines cleared.
   */
  private final LinesCleared my_lines;

  /**
   * The popup-dialog.
   */
  private JDialog my_popup;
  
  /**
   * The image popup element.
   */
  private Element my_image_popup;
  /**
   * The controller popup element.
   */
  private Element my_controller_popup;

  /**
   * Creates a heads up display.
   * @param the_main allows to change states.
   */
  public HeadsUpDisplay(final Main the_main) {
    super();
    my_main = the_main;
    my_timer = new GameTimer(the_main);
    my_score = new ScoreTracker();
    my_level = new Level();
    my_lines = new LinesCleared();
    
  }

  /**
   * Creates a start screen.
   */
  public void startGame() {
    my_nifty.gotoScreen(HUD);
    my_main.setGameState(GameState.ACTIVE);
  }

  /**
   * Restarts the game.
   */
  @SuppressWarnings("static-access")
  public void restartGame() {
    my_nifty.closePopup(my_image_popup.getId());
    my_nifty.closePopup(my_controller_popup.getId());
    my_main.setGameState(GameState.CLOSE);
    my_main.restartGame();
  }

  /**
   * Quits the game.
   */
  public void quitGame() {
    my_main.dispose();
  }


  @Override
  public void bind(final Nifty the_nifty, final Screen the_screen) {
    my_nifty = the_nifty;
    my_screen = the_screen;
  }

  /**
   * creates a popup for the control menu.
   */
  public void controlsGame() {
    setDialog("KEYS");
  }

  /**
   * creates a popup for the about menu.
   */
  public void aboutGame() {
    setDialog("ABOUT");
  }

  /**
   * Creates a control menu for the controls.
   * @param the_controls the name of image.
   */
  private void setDialog(final String the_controls) {

    if (my_popup == null || !my_popup.isVisible()) {
      my_popup =
          new Dialog(my_main, "/textures/hud/TETRIS-" + 
              the_controls + ".png");
    }
  }

  @Override
  public void update(final float the_tpf) {
    if (my_nifty.getCurrentScreen().getScreenId().equals(HUD)) {
      my_nifty.getCurrentScreen().findElementByName("score").getRenderer(
                   TextRenderer.class).setText(my_score.toString());
      my_nifty.getCurrentScreen().findElementByName("timer").getRenderer(
                   TextRenderer.class).setText(my_timer.toString());
      my_nifty.getCurrentScreen().findElementByName("level").getRenderer(
                   TextRenderer.class).setText(my_level.toString());
      my_nifty.getCurrentScreen().findElementByName("lines").getRenderer(
                   TextRenderer.class).setText(my_lines.toString());
    }
  }

  /**
   * @return returns the current score.
   */
  public int getScore() {
    return Integer.parseInt(my_score.toString());
  }

  /**
   * @param the_points_addition adds points the score.
   */
  public void setScore(final int the_points_addition) {
    my_score.setScore(Integer.parseInt(my_score.toString()) + 
                      the_points_addition * my_level.getLevel() * MULTIPLIER);
  }

  /**
   * Increases the level.
   */
  public void incrementLevel() {
    my_level.incrementLevel();
  }

  /**
   * @param the_lines adds to the line.
   */
  public void addLines(final int the_lines) {
    my_lines.setLines(the_lines);
  }

  /**
   * Pauses the game (required for nifty).
   */
  public void pauseGame() {
    isGamePaused(true);
  }

  /**
   * Unpauses the game (required for nifty).
   */
  public void unpauseGame() {
    isGamePaused(false);
  }

  /**
   * @param the_game_paused checks to see if the game is paused.
   */
  public void isGamePaused(final boolean the_game_paused) {
    if (the_game_paused) {
      my_image_popup = my_nifty.createPopup("popupExit");
      my_controller_popup = my_nifty.createPopup("popupController");
      // unable to write initialize the popup, this doesn't cause an
      // issue since nifty's gui is created after it is initialized.
      my_nifty.showPopup(my_screen, my_image_popup.getId(), null);
      my_nifty.showPopup(my_screen, my_controller_popup.getId(), null);
      my_main.setGameState(GameState.PAUSED);
    } else {
      my_nifty.closePopup(my_image_popup.getId());
      my_nifty.closePopup(my_controller_popup.getId());
      my_main.setGameState(GameState.ACTIVE);
    }
    my_timer.stopTimer(the_game_paused);
  }

  /**
   * @return returns the amount of lines.
   */
  public int getLines() {
    return my_lines.getLines();
  }

  @Override
  public void onEndScreen() {
    // intentionally left blank
  }

  @Override
  public void onStartScreen() {
    // intentionally left blank
  }
}
