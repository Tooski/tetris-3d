/*
 * Josef Nosov
 * 
 * TCSS 305 - Autumn 2012
 * Tetris Project - Part 4
 * December 06, 2012
 */
package gui.sidebar.piece;

import main.Main;
import tetris.Board;

/**
 * Creates a 3D representation of the piece.
 * @author Josef Nosov
 * @version 12/06/2012
 */
public class CurrentPiece3D extends AbstractPiece3D {

  /**
   * The location of this node.
   */
  private static final float [] LOCATION = 
  {0.8625f, 1f, 
    0.73134328358208955223880597014926f, 
    0.87860696517412935323383084577115f };
  
  /**
   * Creates a 3D representation of the piece.
   * @param the_main the main board.
   * @param the_block_size the size of the blocks.
   * @param the_board the board that is being passed in.
   */
  public CurrentPiece3D(final Main the_main, final float the_block_size, 
                        final Board the_board) {
    super(the_main, the_block_size, PieceValue.CURRENT_PIECE, the_board, LOCATION);
  }


}
