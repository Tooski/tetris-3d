/*
 * Josef Nosov
 * 
 * TCSS 305 - Autumn 2012
 * Tetris Project - Part 4
 * December 06, 2012
 */
package gui.sidebar.piece;

import com.jme3.app.state.AbstractAppState;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Box;
import com.jme3.util.TangentBinormalGenerator;
import main.GameState;
import main.Main;
import tetris.Board;
import tetris.pieces.Piece;

/**
 * Creates a 3D Piece.
 * @author Josef Nosov
 * @version 12/06/2012
 */
public abstract class AbstractPiece3D extends AbstractAppState {

  /**
   * Gives the pieces a shiny texture.
   */
  private static final float SHININESS = 5f;
  
  /**
   * The node's size is reduced by 90%.
   */
  private static final float SCALE_NODE = 0.90f;
  
  /**
   * The Z position of the lighting.
   */
  private static final int Z_POSITION_LIGHTING = -2;
  
  /**
   * Divides the colors so RGBA can use floats.
   */
  private static final int COLOR_DIVISOR = 255;
  
  /**
   * The amount of pieces.
   */
  private static final int PIECES = 4;
  
  /**
   * Index is 3.
   */
  private static final int THIRD_INT = 3;

  /**
   * The size of the block.
   */
  private final float my_block_size;
  
  /**
   * The main frame.
   */
  private final Main my_main;
  
  /**
   * The value of this piece.
   */
  private final PieceValue my_piece;
  
  /**
   * The geometry of this piece.
   */
  private Geometry[] my_block_geom;
  
  /**
   * The material used to color the piece.
   */
  private Material[] my_block_mat;
  /**
   * The node of this piece.
   */
  private Node my_node;
  /**
   * The board.
   */
  private final Board my_board;

  /**
   * Creates a 3D piece to represent the current and next piece.
   * @param the_main the main frame.
   * @param the_block_size the size of the blocks.
   * @param the_piece the pieces.
   * @param the_board the size of the board.
   * @param the_location the location of the piece.
   */
  public AbstractPiece3D(final Main the_main, final float the_block_size, 
                         final PieceValue the_piece,
                 final Board the_board, final float[] the_location) {
    super();
    my_main = the_main;
    my_block_size = the_block_size;
    my_board = the_board;
    my_piece = the_piece;
    final Camera camera = my_main.getCamera().clone();
    camera.setViewPort(the_location[0], the_location[1], the_location[2],
                          the_location[THIRD_INT]);
    final ViewPort view = my_main.getRenderManager().createMainView("piece" + 
        the_piece, camera);
    setNode(view);
    initalize3DBlocks();
  }
  
  
  /**
   * Sets up the node.
   * @param the_view creates a viewport.
   */
  private void setNode(final ViewPort the_view) {
    my_node = new Node();
    final DirectionalLight light = new DirectionalLight();
    light.setDirection(new Vector3f(1, 0, Z_POSITION_LIGHTING).normalizeLocal());
    light.setColor(ColorRGBA.White);
    my_node.addLight(light);
    my_main.getRootNode().attachChild(my_node);
    my_node.setLocalScale(my_node.getLocalScale().getX() * SCALE_NODE, 
                          my_node.getLocalScale().getY() *
      SCALE_NODE, my_node.getLocalScale().getZ() * SCALE_NODE);
    the_view.attachScene(my_node);

  }

  /**
   * Creates the 3d blocks.
   */
  private void initalize3DBlocks() {
    my_block_geom = new Geometry[PIECES];
    my_block_mat = new Material[PIECES];
    for (int i = 0; i < PIECES; i++) {
      final Box b = new Box(Vector3f.ZERO, my_block_size, my_block_size, my_block_size);
      TangentBinormalGenerator.generate(b);
      my_block_geom[i] = new Geometry("Tetris " + i, b);
      my_block_mat[i] =
          new Material(my_main.getAssetManager(), "Common/MatDefs/Light/Lighting.j3md");
      my_block_mat[i].setBoolean("UseMaterialColors", true);
      my_block_mat[i].setFloat("Shininess", SHININESS);
      my_block_mat[i].setTexture("DiffuseMap",
         my_main.getAssetManager().loadTexture("textures/pieces/block.png"));
      my_block_mat[i].setTexture("NormalMap",
         my_main.getAssetManager().loadTexture("textures/pieces/normal_block.png"));
      my_block_geom[i].setMaterial(my_block_mat[i]);
      set3DBlock(i);
      my_node.attachChild(my_block_geom[i]);
    }
  }
  
  /**
   * Updates the sidebar's piece every time a new piece is placed.
   * 
   * @param the_index current index of the piece.
   */
  private void set3DBlock(final int the_index) {
    Piece piece = null;
    switch (my_piece) {
      case CURRENT_PIECE:
        piece = my_board.currentPiece();
        break;
      case NEXT_PIECE:
        piece = my_board.nextPiece();
        break;
      default:
        break;
    }

    if (piece != null) {
      final ColorRGBA color =
          new ColorRGBA((float) piece.color().getRed() / COLOR_DIVISOR, 
                        (float) piece.color().getGreen() / COLOR_DIVISOR, 
                        (float) piece.color().getBlue() / COLOR_DIVISOR,
                        (float) piece.color().getAlpha() / COLOR_DIVISOR);
      my_block_geom[the_index].setLocalTranslation((piece.blocks()[the_index].x() *
                                                         my_block_size * 2) -
                                                            my_block_size * 2,
                                                        my_block_size *
                                                            piece.blocks()[the_index].y() * 2 -
                                                            my_block_size * 2, 0);
      my_block_mat[the_index].setColor("Specular", color);
      my_block_mat[the_index].setColor("Diffuse", color);
    }

  }

  @Override
  public void update(final float the_tpf) {
    super.update(the_tpf);
    if (my_main.getGameState() == GameState.ACTIVE) {
      if (my_board.lastBlocksPlaced() > 0) {
        for (int i = 0; i < PIECES; i++) {
          set3DBlock(i);
        }
      }
      if (my_main.getGameState() == GameState.ACTIVE) {
        my_node.rotate(0, the_tpf, 0);
      }
    }
  }
  
  
  /**
   * 
   * @author Josef Nosov
   *
   */
  protected enum PieceValue {
    /**
     * Current piece.
     */
    CURRENT_PIECE, 
    /**
     * Next Piece.
     */
    NEXT_PIECE;
  }

}
