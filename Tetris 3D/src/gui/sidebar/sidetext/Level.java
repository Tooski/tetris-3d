/*
 * Josef Nosov
 * 
 * TCSS 305 - Autumn 2012
 * Tetris Project - Part 4
 * December 06, 2012
 */
package gui.sidebar.sidetext;

/**
 * The current level of the game.
 * @author Joe
 * @version 12/06/12
 */
public class Level {

  /**
   * The current level.
   */
  private int my_level = 1;

  @Override
  public String toString() {
    return String.valueOf(my_level);
  }

  /**
   * Updates level.
   */
  public void incrementLevel() {
    my_level++;
  }

  /**
   * @return returns the integer value of the level.
   */
  public int getLevel() {
    return my_level;
  }
}
