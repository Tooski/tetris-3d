/*
 * Josef Nosov
 * 
 * TCSS 305 - Autumn 2012
 * Tetris Project - Part 4
 * December 06, 2012
 */
package gui.sidebar.sidetext;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

import main.GameState;
import main.Main;

/**
 * The game timer.
 * @author Josef Nosov
 * @version 12/06/2012
 *
 */
public class GameTimer {

  /**
   * How many milliseconds are in an instant?
   */
  private static final int MILLISECONDS_PER_INSTANCE = 10;
  
  /**
   * How many instances per second?
   */
  private static final int INSTANCES_PER_SECOND = 100;
  
  /**
   * How many seconds per minute?
   */
  private static final int SECONDS_PER_MINUTE = 60;

  /**
   * Divider.
   */
  private static final String DIVIDER = ":";

  /**
   * Instance updates.
   */
  private int my_instances;
  /**
   * Seconds updates.
   */
  private int my_seconds;
  
  /**
   * Minutes update.
   */
  private int my_minutes;
  
  /**
   * The timer.
   */
  private final Timer my_timer;


  /**
   * Creates a game timer.
   * @param the_main checks to see it game is active.
   */
  public GameTimer(final Main the_main) {
    my_timer = new Timer(MILLISECONDS_PER_INSTANCE, new ActionListener() {
      public void actionPerformed(final ActionEvent the_event) {
        if (the_main.getGameState() == GameState.ACTIVE) {
          my_instances += my_timer.getDelay() / MILLISECONDS_PER_INSTANCE;
          if (my_instances % INSTANCES_PER_SECOND == 0) {
            my_seconds++;
            my_instances = 0;
            if (my_seconds % SECONDS_PER_MINUTE == 0) {
              my_minutes++;
              my_seconds = 0;
            }
          }
        } else if (the_main.getGameState() == GameState.CLOSE) {
          my_timer.stop();
        }
      }
    });
    my_timer.start();
  }
  
  /**
   * Stops timer.
   * @param the_timer_is_active if true, the timer will stop, if false, the timer will resume.
   */
  public void stopTimer(final boolean the_timer_is_active) {
    if (the_timer_is_active) {
      my_timer.stop();
    } else {
      my_timer.start();
    }
  }

  /**
   * @return the current time.
   */
  public String toString() {
    return my_minutes + DIVIDER + my_seconds + DIVIDER + my_instances;
  }

}
