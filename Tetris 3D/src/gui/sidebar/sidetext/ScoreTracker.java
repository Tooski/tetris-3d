/*
 * Josef Nosov
 * 
 * TCSS 305 - Autumn 2012
 * Tetris Project - Part 4
 * December 06, 2012
 */

package gui.sidebar.sidetext;


/**
 * Adds to user's score.
 * @author Josef Nosov
 * @version 12/06/2012
 *
 */
public class ScoreTracker {

  /**
   * The player's score.
   */
  private int my_score;

  @Override
  public String toString() {
    return String.valueOf(my_score);
  }

  /**
   * @param the_score adds to user's score.
   */
  public void setScore(final int the_score) {
    my_score = the_score;
  }
}
