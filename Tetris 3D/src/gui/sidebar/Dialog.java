/*
 * Josef Nosov
 * 
 * TCSS 305 - Autumn 2012
 * Tetris Project - Part 4
 * December 06, 2012
 */
package gui.sidebar;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import main.Main;

/**
 * Creates a dialog for the pop-up controller.
 * 
 * @author Joe
 * @version 12/06/2012
 */
@SuppressWarnings("serial")
public class Dialog extends AbstractDialog {

  /**
   * @param the_main the main.
   * @param the_image_filepath the image.
   */
  public Dialog(final Main the_main, final String the_image_filepath) {
    super(the_main, the_image_filepath);
  }
  
  @Override
  protected void createButton() {
    final ImageIcon okay = createImage("/textures/hud/OKAY.png");
    final JButton button =
        new JButton(new ImageIcon(
          okay.getImage().getScaledInstance((int) (okay.getIconWidth() / my_scale),
                               (int) (okay.getIconHeight() / my_scale), Image.SCALE_SMOOTH)));
    button.setBounds((int) ((my_label.getIcon().getIconWidth() / 2 - 
        button.getIcon().getIconWidth() / 2)), 
        (int) ((my_label.getIcon().getIconHeight() - 
            button.getIcon().getIconHeight()) - 
            (button.getIcon().getIconHeight() / DENOMINATOR_SCALE)), 
            button.getIcon().getIconWidth(), 
            button.getIcon().getIconHeight());
    button.setBorder(null);
    button.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent the_arg) {
        dispose();
      }

    });
    add(button);
  }
}
