/*
 * Josef Nosov
 * 
 * TCSS 305 - Autumn 2012
 * Tetris Project - Part 4
 * December 06, 2012
 */

package effects;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Matrix3f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Box;
import com.jme3.util.TangentBinormalGenerator;
import gui.GeomPhysics;
import gui.Panel;
import java.awt.Color;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import main.Main;


/**
 * Creates projectiles.
 * @author Josef Nosov
 * @version 12/06/2012
 *
 */
public abstract class AbstractProjectile {

  
  /**
   * The initial Y position for the projectiles.
   */
  private static final int INITIAL_Y_POSITION = 100;

  /**
   * Divides the color so that RGBA can use floats properly.
   */
  private static final int COLOR_DIVIDER = 255;

  /**
   * Creates the panel.
   */
  protected Panel my_panel;

  /**
   * Creates a main.
   */
  protected Main my_main;

  /**
   * Creates a randomizer.
   */
  protected Random my_rand;

  /**
   * The queue that holds the pieces.
   */
  private final Queue<GeomPhysics> my_boxes;

  /**
   * The amount of blocks being created.
   */
  private final int my_amount_of_blocks;

  /**
   * Creates projectiles.
   * @param the_main the main frame.
   * @param the_panel the node that it will be attached to.
   * @param the_amount_of_blocks the amount of blocks that will be created.
   */
  public AbstractProjectile(final Main the_main, 
                            final Panel the_panel, 
                            final int the_amount_of_blocks) {
    my_panel = the_panel;
    my_main = the_main;
    my_amount_of_blocks = the_amount_of_blocks;
    my_rand = new Random();
    my_boxes = new LinkedList<GeomPhysics>();
    createBoxes();
  }
  
  /**
   * Creates the boxes.
   */
  private void createBoxes() {
    for (int i = 0; i < my_amount_of_blocks; i++) {

      final Box block = new 
          Box(my_panel.getBlockSize(), my_panel.getBlockSize(), my_panel.getBlockSize());

      TangentBinormalGenerator.generate(block);
      final Geometry ball_geo = new Geometry("blocks", block);
      ball_geo.setMaterial(my_panel.createMaterial(ball_geo,
                new Color((float) my_rand.nextInt(COLOR_DIVIDER) / COLOR_DIVIDER,
                          (float) my_rand.nextInt(COLOR_DIVIDER) / COLOR_DIVIDER,
                          (float) my_rand.nextInt(COLOR_DIVIDER) / COLOR_DIVIDER),
                                                1f));

      final RigidBodyControl physics = new RigidBodyControl(10f);
      ball_geo.addControl(physics);
      my_panel.getBulletAppState().getPhysicsSpace().add(physics);
      physics.setPhysicsRotation(new Matrix3f(my_rand.nextFloat(), my_rand.nextFloat(),
                                                 my_rand.nextFloat(), my_rand.nextFloat(),
                                                 my_rand.nextFloat(), my_rand.nextFloat(),
                                                 my_rand.nextFloat(), my_rand.nextFloat(),
                                                 my_rand.nextFloat()));
      my_panel.getCallableThread().attachNode(ball_geo);
      my_boxes.add(new GeomPhysics(physics, ball_geo));
      physics.setPhysicsLocation(new Vector3f(0, -INITIAL_Y_POSITION, 0));

    }
  }

  /**
   * shoots a new projectile.
   */
  public void triggerNextProjectile() {
    final GeomPhysics g = my_boxes.poll();
    randomizePhysics(g.getGeometry(), g.getRigidBodyControl(), getLocation(), getDirection());
    my_boxes.add(g);
  }

  /**
   * @return the location of the camera.
   */
  protected abstract Vector3f getLocation();

  /**
   * @return the direction the camera is facing.
   */
  protected abstract Vector3f getDirection();

  
  /**
   * 
   * @param the_geometry the geometry of the shape.
   * @param the_physics the physics of the shape.
   * @param the_location the location of the shape.
   * @param the_direction the direction of the shape.
   */
  private void randomizePhysics(final Geometry the_geometry, 
                                final RigidBodyControl the_physics, 
                                final Vector3f the_location,
                                final Vector3f the_direction) {

    final ColorRGBA color =
        new ColorRGBA(
                      (float) my_rand.nextInt(COLOR_DIVIDER) / COLOR_DIVIDER, 
                      (float) my_rand.nextInt(COLOR_DIVIDER) / COLOR_DIVIDER,
                      (float) my_rand.nextInt(COLOR_DIVIDER) / COLOR_DIVIDER, 1f);
    the_geometry.getMaterial().setColor("Specular", color);
    the_geometry.getMaterial().setColor("Diffuse", color);

    the_physics.setPhysicsLocation(the_location);
    the_physics.setLinearVelocity(the_direction);
    the_physics.setPhysicsRotation(
        new Matrix3f(my_rand.nextFloat(), my_rand.nextFloat(), 
                     my_rand.nextFloat(), my_rand.nextFloat(), 
                     my_rand.nextFloat(), my_rand.nextFloat(), 
                     my_rand.nextFloat(), my_rand.nextFloat(), 
                     my_rand.nextFloat()));
  }
}
