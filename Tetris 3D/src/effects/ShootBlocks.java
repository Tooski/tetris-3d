/*
 * Josef Nosov
 * 
 * TCSS 305 - Autumn 2012
 * Tetris Project - Part 4
 * December 06, 2012
 */
package effects;
import com.jme3.math.Vector3f;
import gui.Panel;
import main.Main;


/**
 * Creates shootable pieces.
 * @author Josef Nosov
 * @version 12/06/2012
 */
public class ShootBlocks extends AbstractProjectile {

  /**
   * The speed of the projectile.
   */
  private static final float SPEED_OF_PROJECTION = 25f;
  /**
   * Projection's position.
   */
  private static final float NEW_PROJECTILE_POSITION = 0.1f;
  /**
   * Only allows 10 blocks to be added to the queue.
   */
  private static final int AMOUNT_OF_BLOCKS = 10;

  /**
   * Creates shootable pieces.
   * @param the_main the main frame.
   * @param the_panel the panel is where the shapes will be added.
   */
  public ShootBlocks(final Main the_main, final Panel the_panel) {
    super(the_main, the_panel, AMOUNT_OF_BLOCKS);

  }

  @Override
  protected Vector3f getLocation() {
    return my_panel.getCamera().getWorldCoordinates(
              my_main.getInputManager().getCursorPosition(), 0.0f);
  }

  @Override
  protected Vector3f getDirection() {
    return my_panel.getCamera().getWorldCoordinates(
         my_main.getInputManager().getCursorPosition(), 
         NEW_PROJECTILE_POSITION).subtractLocal(
         getLocation()).normalizeLocal().mult(SPEED_OF_PROJECTION);
  }

}
