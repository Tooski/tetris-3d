/*
 * Josef Nosov
 * 
 * TCSS 305 - Autumn 2012
 * Tetris Project - Part 4
 * December 06, 2012
 */
package main;

/**
 * 
 * @author Josef Nosov
 * @version 12/06/2012
 */
public enum GameState {
  /**
   * Creates an active game.
   */
  ACTIVE, 
  /**
   * Game over.
   */
  GAME_OVER, 
  /**
   * Game is paused.
   */
  PAUSED, 
  /**
   * Game is reset.
   */
  RESET, 
  /**
   * Start menu.
   */
  START_MENU, 
  /**
   * Game is closing.
   */
  CLOSE;
}
