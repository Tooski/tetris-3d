/*
 * Josef Nosov
 * 
 * TCSS 305 - Autumn 2012
 * Tetris Project - Part 4
 * December 06, 2012
 */
package main;

import com.jme3.app.SimpleApplication;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.system.JmeCanvasContext;

import de.lessvoid.nifty.Nifty;
import gui.Panel;
import gui.sidebar.GameOverDialog;
import gui.sidebar.HeadsUpDisplay;
import gui.sidebar.piece.CurrentPiece3D;
import gui.sidebar.piece.NextPiece3D;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.Timer;

import tetris.Board;

/**
 * The main method of the tetris game.
 * @author Josef Nosov
 * @version 12/06/2012
 *
 */
public class Main extends SimpleApplication 
  implements java.awt.event.ActionListener, Observer {


  
  /**
   * Multiplies the current delay time by 0.95, (ie: initial time is 1000 ms,
   * the next level the time will be 950).
   */
  private static final float DELAY_MULTIPLIER = 0.95f;
  /**
   * Maximum delay in milliseconds.
   */
  private static final int MAXIMUM_DELAY = 300;
  /**
   * Start Key.
   */
  private static final String START = "Start";
  /**
   * Left Key.
   */
  private static final String LEFT = "Left";
  /**
   * Right Key.
   */
  private static final String RIGHT = "Right";
  /**
   * Up Key.
   */
  private static final String UP = "Up";
  /**
   * Down Key.
   */
  private static final String DOWN = "Down";
  /**
   * Pause Key.
   */
  private static final String PAUSE = "Pause";
  /**
   * Drop Key.
   */
  private static final String DROP = "Drop";

  /**
   * The board's width.
   */
  private static final int BOARD_WIDTH = 10;

  /**
   * The board's height.
   */
  private static final int BOARD_HEIGHT = 20;
  
  
  /**
   * The size of the pieces.
   */
  private static final float MULTIPLIER = 1f;


  /**
   * How many lines required for next level.
   */
  private static final int NEXT_LEVEL = 5;

  /**
   * Initial time for the timer.
   */
  private static final int INITIAL_TIME = 1000;
  /**
   * Initial Screen width.
   */
  private static final int FRAME_WIDTH = 640;

  /**
   * Initial Screen height.
   */
  private static final int FRAME_HEIGHT = 480;

  /**
   * The canvas that renders the 3D.
   */
  private static JmeCanvasContext my_canvas;

  /**
   * The main tetris game.
   */
  private static Main my_tetris;

  /**
   * The frame that holds the 3D canvas.
   */
  private static JFrame my_frame;
  
  /**
   * The game's GUI.
   */
  private HeadsUpDisplay my_gui_menu;

  /**
   * The tetris panel in 3D.
   */
  private Panel my_panel;

  /**
   * The board for tetris (what makes the game run).
   */
  private final Board my_board;

  /**
   * The timer for the game.
   */
  private Timer my_timer;

  /**
   * The game's state.
   */
  private GameState my_current_state;

  /**
   * The amount of lines previously placed.
   */
  private int my_lines_placed;
  
  /**
   * The action listener.
   */
  private final ActionListener my_action_listener = new ActionListener() {
    public void onAction(final String the_action, final boolean the_action_pressed,
                         final float the_tpf) {
      if (the_action_pressed) {
        if (my_current_state == GameState.ACTIVE) {
          activeKeys(the_action);
        }
        otherKeys(the_action);
      }
    }

  };

  /**
   * Initializes main, creates board and observer.
   */
  public Main() {
    super();
    final Random random = new Random();
    my_board = new Board(BOARD_HEIGHT, BOARD_WIDTH, random.nextLong());
    my_current_state = GameState.START_MENU;
    my_board.addObserver(this);

  }

  /**
   * Initializes the tetris game.
   * 
   * @param the_args is the args.
   */
  public static void main(final String[] the_args) {
    /*
     * Ignores information dialog that JMonkey produces example: ignores console
     * output whenever object is added or detached to node. Only prints out
     * severe and necessary lines to console.
     */
    Logger.getLogger("").setLevel(Level.SEVERE);

    my_frame = new JFrame("Tetris 3D");
    my_frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    my_frame.setVisible(true);
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        my_tetris = new Main();
        my_tetris.createCanvas();
        my_canvas = (JmeCanvasContext) my_tetris.getContext();

        my_canvas.setSystemListener(my_tetris);
        my_canvas.getCanvas().setPreferredSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
        my_tetris.startCanvas();
        my_frame.add(my_canvas.getCanvas());
        my_frame.pack();

      }
    });
  }

  /**
   * @return returns the canvas' width.
   */
  public final int getScreenWidth() {
    return my_canvas.getCanvas().getWidth();
  }
  
  /**
   * @return returns the canvas' height.
   */
  public final int getScreenHeight() {
    return my_canvas.getCanvas().getHeight();
  }

  @Override
  public void update(final Observable the_obj, final Object the_arg) {
    if (((Board) the_obj).lastBlocksPlaced() > 0) {

      my_panel.piecesPlaced();
    }

    if (((Board) the_obj).lastLinesRemoved() > 0) {
      my_lines_placed += my_board.lastLinesRemoved();
      my_gui_menu.setScore(my_board.lastLinesRemoved() * my_board.lastLinesRemoved());
      my_gui_menu.addLines(my_board.lastLinesRemoved());
    }
    if (((Board) the_obj).isFull()) {

      my_current_state = GameState.GAME_OVER;
      my_panel.gameOver();
      final Main m = this;

      /*
       * Used to prevent thread concurrence, creates a popup for "game over".
       */
      enqueue(new Callable<Object>() {
        public Object call() {
          return new GameOverDialog(m, "/textures/hud/TETRIS-GAMEOVER.png");
        }
      });
    }
  }

  @Override
  public void simpleInitApp() {
    my_panel = new Panel(my_board, this);
    getStateManager().attach(my_panel);
    getViewPort().clearScenes();
    setDisplayStatView(false);
    setDisplayFps(false);
    flyCam.setEnabled(false);
    final String[] controls = {START, LEFT, RIGHT, UP, DOWN, PAUSE, DROP};
    final KeyTrigger[][] keys =
    {{new KeyTrigger(KeyInput.KEY_RETURN), new KeyTrigger(KeyInput.KEY_SPACE)},
      {new KeyTrigger(KeyInput.KEY_A), new KeyTrigger(KeyInput.KEY_LEFT)},
      {new KeyTrigger(KeyInput.KEY_D), new KeyTrigger(KeyInput.KEY_RIGHT)},
      {new KeyTrigger(KeyInput.KEY_W), new KeyTrigger(KeyInput.KEY_UP)},
      {new KeyTrigger(KeyInput.KEY_S), new KeyTrigger(KeyInput.KEY_DOWN)},
      {new KeyTrigger(KeyInput.KEY_P)}, {new KeyTrigger(KeyInput.KEY_SPACE)}};
    for (int i = 0; i < controls.length; i++) {
      inputManager.addMapping(controls[i], keys[i]);
      inputManager.addListener(my_action_listener, controls[i]);
    }
    my_current_state = GameState.START_MENU;
    my_timer = new Timer(INITIAL_TIME, this);
    my_timer.start();
    my_frame.addComponentListener(new ComponentAdapter() {
      public void componentResized(final ComponentEvent the_arg) {
        if (getGameState() == GameState.ACTIVE) {
          my_gui_menu.isGamePaused(true);
        }
      }
    });
    my_frame.addWindowStateListener(new WindowStateListener() {
      public void windowStateChanged(final WindowEvent the_arg) {
        if (getGameState() == GameState.ACTIVE) {
          my_gui_menu.isGamePaused(true);
        }
      }
    });
    
    my_gui_menu = new HeadsUpDisplay(this);
    getStateManager().attach(my_gui_menu);
    final NiftyJmeDisplay display =
        new NiftyJmeDisplay(getAssetManager(), getInputManager(), getAudioRenderer(),
                            getGuiViewPort());
    final Nifty nifty = display.getNifty();
    getGuiViewPort().addProcessor(display);
    nifty.fromXml("textures/hud/tetris_hud.xml", "start", my_gui_menu);
    getStateManager().attach(new CurrentPiece3D(this, MULTIPLIER, my_board));
    getStateManager().attach(new NextPiece3D(this, MULTIPLIER, my_board));
    getViewPort().clearScenes();

    
  }

  /**
   * @return Gets the game's current state.
   */
  public GameState getGameState() {
    return my_current_state;
  }


  /**
   * Creates the active keys for the listener.
   * @param the_action is the key that is being pressed.
   */
  private void activeKeys(final String the_action) {
    switch (the_action) {
      case UP:
        my_board.rotateClockwise();
        break;
      case RIGHT:
        my_board.moveRight();
        break;
      case LEFT:
        my_board.moveLeft();
        break;
      case DOWN:
        my_board.moveDown();
        break;
      case DROP:
        my_board.drop();
        break;
      default:
        break;
    }
  }

  /**
   * Creates pause and Start buttons for the listener.
   * @param the_action is the key that is being pressed.
   */
  private void otherKeys(final String the_action) {
    switch (the_action) {
      case PAUSE:
        if (my_current_state == GameState.ACTIVE) {
          my_current_state = GameState.PAUSED;
          my_gui_menu.isGamePaused(true);

          break;
        } else if (my_current_state == GameState.PAUSED) {
          my_current_state = GameState.ACTIVE;
          my_gui_menu.isGamePaused(false);
          break;
        }
        break;
      case START:
        if (my_current_state == GameState.PAUSED) {
          my_current_state = GameState.ACTIVE;
          my_gui_menu.isGamePaused(false);
          break;
        } else if (my_current_state == GameState.START_MENU) {
          my_gui_menu.startGame();
          break;
        }
        break;
      default:
        break;
    }
  }

  /**
   * @return returns the frame.
   */
  public JFrame getFrame() {
    return my_frame;
  }

  @Override
  public void actionPerformed(final ActionEvent the_event) {
    if (my_current_state == GameState.ACTIVE) {
      if (my_lines_placed >= NEXT_LEVEL) {
        if (my_timer.getDelay() >= MAXIMUM_DELAY) {
          my_timer.setDelay((int) (my_timer.getDelay() * DELAY_MULTIPLIER));
        }
        my_gui_menu.incrementLevel();
        my_lines_placed -= NEXT_LEVEL;
      }
      my_board.moveDown();
    } else if (my_current_state == GameState.CLOSE) {
      my_timer.stop();
    }
  }

  /**
   * @param the_new_state changes the game state.
   */
  public void setGameState(final GameState the_new_state) {
    my_current_state = the_new_state;
  }

  /**
   * Closes the game.
   */
  public void dispose() {
    requestClose(true);
    my_current_state = GameState.CLOSE; // disables timers.
    my_frame.setVisible(false);
    my_frame.dispose();
  }

  /**
   * Restarts the game.
   */
  public static void restartGame() {
    my_tetris.setGameState(GameState.CLOSE); // disables timers.
    my_canvas.getCanvas().setSize(0, 0);    
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        my_canvas.destroy(true);      // placed canvas in thread to
        my_tetris.requestClose(true); // prevent thread locks.

        my_tetris = new Main();
        my_tetris.createCanvas();
        my_canvas = (JmeCanvasContext) my_tetris.getContext();
        my_canvas.setSystemListener(my_tetris);
        my_tetris.startCanvas();
        my_frame.add(my_canvas.getCanvas());
        my_canvas.getCanvas().setPreferredSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
        my_frame.pack();
      }
    });
  }

  /**
   * @return returns the delay time.
   */
  public int delay() {
    return my_timer.getDelay();

  }

}
