package tetris.pieces;

import java.util.Map;
import java.util.TreeMap;



/**
 * Creates a piece.
 * @author Josef Nosov
 * @version 10/27/2012
 */
public abstract class AbstractPiece implements MutablePiece {

  /**
   * Stores the piece's size (length * width)/2.
   */
  private final int my_piece_size;

  /**
   * The pieces x position.
   */
  private int my_piece_x;

  /**
   * The pieces y position.
   */
  private int my_piece_y;

  /**
   * Holds the position of a block.
   */
  private final Position my_block_position = new Position();

  /**
   * Stores the piece, and each individual block and position.
   */
  private final Map<Position, Block> my_piece = 
      new TreeMap<Position, Block>(new PositionComparator());


  /**
   * 
   * @param the_piece is the piece.
   */
  public AbstractPiece(final boolean[][] the_piece) {
    my_piece_size = (the_piece.length + the_piece[the_piece.length - 1].length) / 2;

    for (int i = 0; i < my_piece_size; i++) {
      for (int j = 0; j < my_piece_size; j++) {
        my_piece.put(new Position(i, j), new Block(the_piece[i][j]));
      }
    }
  }


  /**
   * Moves the piece left one space.
   */
  public void moveLeft() {
    my_piece_x--;
  }

  /**
   * Moves the piece right one space.
   */
  public void moveRight() {
    my_piece_x++;
  }

  /**
   * Moves the piece down one space.
   */
  public void moveDown() {
    my_piece_y--;
  }


  /**
   * Rotates the piece.
   */
  @SuppressWarnings("unchecked")
  public void rotate() {
    final Map<Position, Block> new_block_position = 
        (Map<Position, Block>) ((TreeMap<Position, Block>) my_piece).clone();
    for (Position current_position : my_piece.keySet()) {
      my_block_position.setXY((my_piece_size - 1) - current_position.y(), 
                              current_position.x());
      my_piece.put(my_block_position, 
                   new_block_position.get(current_position));
    }
  }

  /**
   * @return returns the x position of this piece.
   */
  public int x() {
    return my_piece_x;
  }

  /**
   * @return returns the y position of this piece.
   */
  public int y() {
    return my_piece_y;
  }

  /**
   * Prints a string representation of this piece.
   * @return returns the string representation of this piece.
   */
  public String toString() {
    int i = 0;
    final StringBuilder sb = new StringBuilder();
    for (Position b : my_piece.keySet()) {
      if (i % my_piece_size == 0) {
        sb.append("\n");
      }
      sb.append(my_piece.get(b).toString());
      i++;
    }
    return sb.toString();
  }

  /**
   * @param the_other is the other object being compared.
   * @return returns true or false if they're equal.
   */
  public boolean equals(final Object the_other) {
    boolean result = false;
    if (this == the_other) {
      result = true;
    } else if (the_other != null && the_other.getClass() == getClass()) {
      result = 
          ((AbstractPiece) the_other).x() == x() && 
          ((AbstractPiece) the_other).y() == y() && 
          (my_piece.keySet().toString().equals(((AbstractPiece) 
              the_other).my_piece.keySet().toString()) && 
              my_piece.values().toString().equals(((AbstractPiece) 
                  the_other).my_piece.values().toString()));
    }
    return result;
  }

  /**
   * HashCode method replaces original method.
   * @return returns a new hashcode because equals has been replaced.
   */
  public int hashCode() {
    return toString().hashCode();
  }
}