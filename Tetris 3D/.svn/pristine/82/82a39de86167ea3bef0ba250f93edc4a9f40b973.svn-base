/*
 * Josef Nosov
 * 
 * TCSS 305 - Autumn 2012
 * Tetris Project - Part 4
 * December 06, 2012
 */

package gui;
import com.jme3.app.state.AbstractAppState;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.renderer.ViewPort;
import com.jme3.renderer.queue.RenderQueue.Bucket;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;
import com.jme3.util.SkyFactory;
import com.jme3.util.TangentBinormalGenerator;
import effects.ExplodeBlocks;
import effects.ShootBlocks;
import java.awt.Color;
import java.util.HashMap;
import java.util.Map;
import main.GameState;
import main.Main;
import tetris.Board;
import tetris.util.Point;

/**
 * Creates the panel.
 * @author Josef Nosov
 * @version 12/06/2012
 *
 */
public class Panel extends AbstractAppState {

  /**
   * The color divider.
   */
  private static final int COLOR_DIVISOR = 255;

  /**
   * The background speed.
   */
  private static final int BACKGROUND_SPEED = 150;

  /**
   * The shininess of the pieces.
   */
  private static final float SHININESS = 10f;
  
  /**
   * The transparency alpha.
   */
  private static final float TRANSPARENT_ALPHA = 0.5f;

  /**
   * The Z position.
   */
  private static final int LIGHT_Z_POSITION = -2;

  /**
   * Shoot key.
   */
  private static final String SHOOT = "Shoot";

  /**
   * Reset key.
   */
  private static final String RESET = "Reset";

  /**
   * Hold key.
   */
  private static final String HOLD = "Hold";

  /**
   * The amount of pieces.
   */
  private static final int PIECES = 4;

  /**
   * The size of the pieces.
   */
  private static final float MULTIPLIER = 0.175f;
  
  /**
   * The board.
   */
  private final Board my_board;
  
  /**
   * The node of the piece, main node.
   */
  private final Node my_node;
  
  /**
   * The main frame.
   */
  private final Main my_main;
  
  /**
   * Holds the material for the board.
   */
  private Material[][] my_tetris_mats;
  
  /**
   * Holds the shape for the board.
   */
  private Geometry[][] my_tetris_geom;

  /**
   * Holds the geometry of the current piece.
   */
  private Geometry[] my_current_block_geom;

  /**
   * Holds the materials of the current piece.
   */
  private Material[] my_current_block_mats;
  
  /**
   * Holds the geometry of the projected piece.
   */
  private Geometry[] my_projected_block_geom;
  
  /**
   * Holds the material of the projected piece.
   */
  private Material[] my_projected_block_mats;


  /**
   * The tetris board's physics.
   */
  private final Map<Geometry, GeomPhysics> my_tetris_board;

  /**
   * Creates shootable blocks.
   */
  private ShootBlocks my_shootable;
  
  /**
   * Creates explodable blocks.
   */
  private ExplodeBlocks my_explodable;

  /**
   * The action listener for the mouse/keyboard.
   */
  private final ActionListener my_action_listener = new ActionListener() {

    @Override
    public void onAction(final String the_name, final boolean the_key_pressed,
                         final float the_arg) {
      if (the_name.equals(HOLD)) {
        my_mouse_coords = new Vector2f(my_main.getInputManager().getCursorPosition());
        my_press_check = the_key_pressed;
      }

      if (the_name.equals(RESET)) {
        my_node.setLocalRotation(my_node_original_position);
      }
      if (the_name.equals(SHOOT) && !the_key_pressed &&
          my_main.getGameState() != GameState.PAUSED) {
        my_shootable.triggerNextProjectile();
      }
    }
  };


  /**
   * The camera for the node.
   */
  private final Camera my_camera;
  
  /**
   * Creates callable node to prevent concurrency.
   */
  private final CallableNodes my_thread_nodes;
  /**
   * Creates a physics generator.
   */
  private final BulletAppState my_physics_state;
  
  /**
   * Saves the position of the node.
   */
  private final Quaternion my_node_original_position;

  
  /**
   * The coordinates of the mouse.
   */
  private Vector2f my_mouse_coords = Vector2f.ZERO;

  /**
   * Checks to see if the mouse is pressed.
   */
  private boolean my_press_check;

  /**
   * Creates the skybox.
   */
  private Spatial my_skybox;

  /**
   * Creates a 3D panel.
   * @param the_board is the board.
   * @param the_main is the main frame.
   */
  public Panel(final Board the_board, final Main the_main) {
    super();
    my_tetris_board = new HashMap<Geometry, GeomPhysics>();
    my_physics_state = new BulletAppState();
    my_main = the_main;
    my_board = the_board;
    my_node = new Node();
    my_thread_nodes = new CallableNodes(the_main, my_node);
    my_node_original_position = my_node.getLocalRotation().clone();
    my_camera = my_main.getCamera().clone();
    createGame();
  }
  
  /**
   * Sets up the game.
   */
  private void createGame() {
    final ViewPort view = my_main.getRenderManager().createMainView("tetris", my_camera);
    my_main.getStateManager().attach(my_physics_state);

    createMapping();
    createBoard();
    createBackBoard();
    createPieces();
    createNode(view);
  }
  
  
  /**
   * Creates keymaps.
   */
  private void createMapping() {
    my_main.getInputManager().addMapping(HOLD, new KeyTrigger(KeyInput.KEY_LSHIFT),
                                          new KeyTrigger(KeyInput.KEY_RSHIFT));
    my_main.getInputManager().addMapping(RESET, new KeyTrigger(KeyInput.KEY_Z));
    my_main.getInputManager().addListener(my_action_listener, new String[] {RESET, HOLD});
  }
  
  /**
   * Creats the node.
   * @param the_view the viewport.
   */
  private void createNode(final ViewPort the_view) {

    final DirectionalLight sun = new DirectionalLight();
    sun.setDirection(new Vector3f(1, 0, LIGHT_Z_POSITION).normalizeLocal());
    sun.setColor(ColorRGBA.White);
    my_node.addLight(sun);
    the_view.attachScene(my_node);
    my_skybox = SkyFactory.createSky(my_main.getAssetManager(), 
                                     "textures/hud/BrightSky.dds", false);
    my_node.attachChild(my_skybox);
    my_main.getRootNode().attachChild(my_node);
    my_main.getInputManager().addMapping(SHOOT,
                                         new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
    my_main.getInputManager().addListener(my_action_listener, SHOOT);
    my_explodable = new ExplodeBlocks(my_main, this, my_board);
    my_shootable = new ShootBlocks(my_main, this);
  }
  
  /**
   * Creates the pieces.
   */
  private void createPieces() {
    my_current_block_geom = new Geometry[PIECES];
    my_current_block_mats = new Material[PIECES];
    my_projected_block_geom = new Geometry[PIECES];
    my_projected_block_mats = new Material[PIECES];
    for (int i = 0; i < PIECES; i++) {
      final Point current_piece = my_board.currentPiece().absolutePosition(i);
      final Point projected_piece = my_board.projection().absolutePosition(i);
      my_current_block_geom[i] = createGeometry(current_piece.x(), current_piece.y());
      my_current_block_mats[i] =
          createMats(my_current_block_geom[i], my_board.currentPiece().color(), 1f);
      my_projected_block_geom[i] = createGeometry(projected_piece.x(), projected_piece.y());
      my_projected_block_mats[i] =
          createMats(my_projected_block_geom[i], 
                     my_board.projection().color(), TRANSPARENT_ALPHA);

      my_projected_block_geom[i].setQueueBucket(Bucket.Transparent);

      my_thread_nodes.attachNode(my_current_block_geom[i]);
      my_thread_nodes.attachNode(my_projected_block_geom[i]);
      createPhysics(my_current_block_geom[i]);
    }
  }
  
  /**
   * Creates the backboard.
   */
  private void createBackBoard() {
    final Box b =
        new Box(MULTIPLIER * my_board.width() * 0.99f, MULTIPLIER * my_board.height() * 0.99f,
                MULTIPLIER / 2 * 0.99f);
    final Geometry geom = new Geometry("Border", b);
    final Material mat = 
        new Material(my_main.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
    mat.setColor("Color", new ColorRGBA(0, 0, 0, TRANSPARENT_ALPHA));

    geom.setLocalTranslation(-MULTIPLIER, -MULTIPLIER, MULTIPLIER);

    mat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
    geom.setMaterial(mat);
    geom.setQueueBucket(Bucket.Transparent);
    my_thread_nodes.attachNode(geom);
  }
  
  /**
   * Creates the board.
   */
  private void createBoard() {
    my_tetris_mats = new Material[my_board.height()][my_board.width()];
    my_tetris_geom = new Geometry[my_board.height()][my_board.width()];
    for (int y = my_board.height() - 1; y >= 0; y--) {
      for (int x = 0; x < my_board.width(); x++) {
        my_tetris_geom[y][x] = createGeometry(x, y);
        my_tetris_mats[y][x] = createMats(my_tetris_geom[y][x], my_board.rowAt(y)[x], 1f);
        createPhysics(my_tetris_geom[y][x]);
        enablePhysics(my_tetris_board.get(my_tetris_geom[y][x]).getRigidBodyControl(), false);
      }
    }
  }



  /**
   * Creates physics for this piece.
   * @param the_spartial is the sparial that is being thrown.
   */
  private void createPhysics(final Spatial the_spartial) {
    final RigidBodyControl block_collision = new RigidBodyControl(0f);
    the_spartial.addControl(block_collision);
    block_collision.setKinematic(true);
    my_physics_state.getPhysicsSpace().add(block_collision);
    my_tetris_board.put((Geometry) the_spartial,
                     new GeomPhysics(block_collision, (Geometry) the_spartial));
  }

  
  /**
   * Enables/disables the physics.
   * @param the_control is the physics body.
   * @param the_enable true if enabled, false if disabled.
   */
  private void enablePhysics(final RigidBodyControl the_control, final boolean the_enable) {
    the_control.setEnabled(the_enable);
  }

  /**
   * Creates a new geometry.
   * @param the_x the y position of the block.
   * @param the_y the x position of the block.
   * @return the new geometry.
   */
  private Geometry createGeometry(final int the_x, final int the_y) {
    final Box b = new Box(Vector3f.ZERO, MULTIPLIER, MULTIPLIER, MULTIPLIER);
    final Geometry geom = new Geometry("Square", b);
    TangentBinormalGenerator.generate(b);
    geom.setLocalTranslation(the_x * MULTIPLIER * 2 - 
        (MULTIPLIER * my_board.width()), (the_y * MULTIPLIER * 2) - 
        MULTIPLIER * my_board.height(), MULTIPLIER);
    return geom;
  }

  /**
   * Creates new materials.
   * @param the_geometry that will be used for the shape.
   * @param the_color the color of the shape.
   * @param the_alpha 0 is transparent, 1 is solid.
   * @return returns the material.
   */
  private Material createMats(final Geometry the_geometry, 
                              final Color the_color, final float the_alpha) {
    final Material mats =
        new Material(my_main.getAssetManager(), "Common/MatDefs/Light/Lighting.j3md");
    mats.setTexture("DiffuseMap",
                    my_main.getAssetManager().loadTexture("textures/pieces/block.png"));
    mats.setTexture("NormalMap",
                    my_main.getAssetManager().loadTexture("textures/pieces/normal_block.png"));
    mats.setBoolean("UseMaterialColors", true);
    mats.setFloat("Shininess", SHININESS);
    setColor(mats, the_color, the_alpha);
    the_geometry.setMaterial(mats);
    return mats;
  }

  /**
   * Creates the material.
   * @param the_geometry that will be used for the shape.
   * @param the_color the color of the shape.
   * @param the_alpha 0 is transparent, 1 is solid.
   * @return returns the material.
   */
  public Material createMaterial(final Geometry the_geometry, 
                                 final Color the_color, final float the_alpha) {
    return createMats(the_geometry, the_color, the_alpha);
  }



  @Override
  public void update(final float the_tpf) {
    super.update(the_tpf);
    if (my_main.getGameState() == GameState.ACTIVE) {
      if (my_press_check) {

        final Vector2f current_position = my_main.getInputManager().getCursorPosition();
        final Vector2f movement = current_position.subtract(my_mouse_coords);
        my_node.rotate(0, movement.getX() * the_tpf, 0);

        my_mouse_coords = my_main.getInputManager().getCursorPosition().clone();
      }

      my_skybox.rotate(0, the_tpf / my_main.delay() * BACKGROUND_SPEED, 0);

      for (int i = 0; i < PIECES; i++) {
        final Point current_piece = my_board.currentPiece().absolutePosition(i);
        final Point projected_piece = my_board.projection().absolutePosition(i);

        if (my_board.projection().absolutePosition(i).y() >= my_board.height()) {
          my_thread_nodes.detachNode(my_projected_block_geom[i]);
        } else if (!my_node.hasChild(my_projected_block_geom[i])) {
          my_thread_nodes.attachNode(my_projected_block_geom[i]);
        }
        if (current_piece.y() < my_board.height()) {
          my_thread_nodes.attachNode(my_current_block_geom[i]);
          movePiece(my_current_block_geom[i], current_piece);
        } else {
          my_thread_nodes.detachNode(my_current_block_geom[i]);
        }
        movePiece(my_projected_block_geom[i], projected_piece);

      }
    }
  }

  /**
   * Moves the piece.
   * @param the_geom is the geometry of the shape.
   * @param the_point is the position of the piece.
   */
  private void movePiece(final Geometry the_geom, final Point the_point) {
    the_geom.setLocalTranslation(the_point.x() * MULTIPLIER * 2 - 
                             my_board.width() * MULTIPLIER,
                             the_point.y() * MULTIPLIER * 2 - 
                             my_board.height() * MULTIPLIER, 
                             the_geom.getLocalTranslation().getZ());
  }

  /**
   * Sets a new color.
   * @param the_material the material.
   * @param the_color the selected color.
   * @param the_alpha is the alpha, 0 is transparent, 1 is solid.
   */
  private void setColor(final Material the_material, 
                        final Color the_color, final float the_alpha) {
    if (the_color != null) {
      final ColorRGBA color =
          new ColorRGBA((float) the_color.getRed() / COLOR_DIVISOR, 
                        (float) the_color.getGreen() / COLOR_DIVISOR,
                        (float) the_color.getBlue() / COLOR_DIVISOR, 
                        the_alpha);
      the_material.setColor("Specular", color);
      the_material.setColor("Diffuse", color);
      if (the_alpha < 1f) {
        the_material.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
      }
    }
  }

  /**
   * Updates the position of the piece and the board.
   */
  private void updatePiece() {
    updateBoard();
    for (int i = 0; i < PIECES; i++) {
      setColor(my_current_block_mats[i], my_board.currentPiece().color(), 1f);
      setColor(my_projected_block_mats[i], my_board.projection().color(), TRANSPARENT_ALPHA);

    }
  }

  /**
   * Updates the board.
   */
  private void updateBoard() {
    if (my_board.lastLinesRemoved() > 0) {
      for (int i = 0; i < my_board.lastLinesRemoved() * my_board.width(); i++) {
        my_explodable.triggerNextProjectile();
      }
    }
    for (int y = my_board.height() - 1; y >= 0; y--) {
      for (int x = 0; x < my_board.width(); x++) {
        if (my_board.rowAt(y)[x] == null) {
          my_thread_nodes.detachNode(my_tetris_geom[y][x]);
          enablePhysics(my_tetris_board.get(my_tetris_geom[y][x]).getRigidBodyControl(), 
                        false);
        } else {
          setColor(my_tetris_mats[y][x], my_board.rowAt(y)[x], 1f);
          my_thread_nodes.attachNode(my_tetris_geom[y][x]);

          enablePhysics(my_tetris_board.get(my_tetris_geom[y][x]).getRigidBodyControl(), true);
        }
      }
    }
  }

  /**
   * Updates the pieces.
   */
  public void piecesPlaced() {
    updatePiece();
  }

  /**
   * @return returns the block size.
   */
  public float getBlockSize() {
    return MULTIPLIER;
  }

  /**
   * @return gets the camera's position.
   */
  public Camera getCamera() {
    return my_camera;
  }

  /**
   * @return gets the bullet state.
   */
  public BulletAppState getBulletAppState() {
    return my_physics_state;
  }
  /**
   * @return gets the node's failsafe thread.
   */
  public CallableNodes getCallableThread() {
    return my_thread_nodes;
  }


  /**
   * If the gameover occurs, background is shootable.
   */
  public void gameOver() {
    //Removed the node reset position once the game ends.
    for (GeomPhysics f : my_tetris_board.values()) {
      final RigidBodyControl g = f.getRigidBodyControl();
      g.setMass(1);
      g.setKinematic(false);
    }
    for (int i = 0; i < PIECES; i++) {
      if (my_board.projection().absolutePosition(i).y() >= 
          my_board.height()) {
        my_thread_nodes.detachNode(my_projected_block_geom[i]);
      }
      my_thread_nodes.detachNode(my_current_block_geom[i]);
    }
  }
}
