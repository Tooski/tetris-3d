
package gui.hud;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import main.Main;

/**
 * Creates a dialog for the pop-up controller.
 * 
 * @author Josef Nosov
 * @version 12/06/2012
 */
@SuppressWarnings("serial")
public class GameOverDialog extends AbstractDialog {

  /**
   * Used to divide by 10.
   */
  private static final int DIVIDES_BY_TEN = 10;
  /**
   * Used to divide by 3.
   */
  private static final int DIVIDES_BY_THREE = 3;
  /**
   * The main frame.
   */
  private final Main my_main;


  /**
   * 
   * @param the_main the background frame.
   * @param the_image creates an image for the background.
   */
  public GameOverDialog(final Main the_main, final ImageIcon the_image) {
    super(the_main, the_image);
    my_main = the_main;

  }

  @Override
  protected void createButton() {
    final ImageIcon okay = new ImageIcon("assets/textures/hud/OKAY.png");

    final JButton okay_button =
        new JButton(new ImageIcon(okay.getImage().getScaledInstance(
                    (int) (okay.getIconWidth() / my_scale),
                               (int) (okay.getIconHeight() / my_scale), Image.SCALE_SMOOTH)));
    okay_button.setBounds((int) ((my_label.getIcon().getIconWidth() / 2 -
          okay_button.getIcon().getIconWidth() - okay_button.getIcon().getIconWidth() / 
          DIVIDES_BY_TEN)),
          (int) ((my_label.getIcon().getIconHeight() - 
              okay_button.getIcon().getIconHeight()) - 
              (okay_button.getIcon().getIconHeight() / DIVIDES_BY_THREE)), 
              okay_button.getIcon().getIconWidth(),
                   okay_button.getIcon().getIconHeight());
    okay_button.setBorder(null);

    okay_button.addActionListener(new ActionListener() {

      @SuppressWarnings("static-access")
      public void actionPerformed(final ActionEvent the_args) {
        dispose();
        my_main.restartGame();
      }

    });
    add(okay_button);

    final ImageIcon nope = new ImageIcon("assets/textures/hud/NOPE.png");

    final JButton nope_button =
        new JButton(new ImageIcon(nope.getImage().getScaledInstance(
                     (int) (nope.getIconWidth() / my_scale),
                               (int) (nope.getIconHeight() / my_scale), Image.SCALE_SMOOTH)));
    nope_button.setBounds(
      (int) ((my_label.getIcon().getIconWidth() / 2 + okay_button.getIcon().getIconWidth() / 
          DIVIDES_BY_TEN)),
                   (int) ((my_label.getIcon().getIconHeight() - 
       nope_button.getIcon().getIconHeight()) - 
       (nope_button.getIcon().getIconHeight() / DIVIDES_BY_THREE)),
         nope_button.getIcon().getIconWidth(), nope_button.getIcon().getIconHeight());
    nope_button.setBorder(null);

    nope_button.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent the_args) {
        dispose();
        my_main.dispose();
      }

    });

    add(nope_button);
  }
}
