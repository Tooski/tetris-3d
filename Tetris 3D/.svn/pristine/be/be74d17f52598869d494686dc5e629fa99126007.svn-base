
package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import main.Main;

import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Matrix3f;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Box;
import com.jme3.util.TangentBinormalGenerator;

import tetris.Board;
import tetris.pieces.Piece;
import tetris.util.Point;

public class Panel {

  private static float MULTIPLIER = .175f;
  Board my_board;
  private Camera my_camera;
  private Node node;
  private ViewPort view;  Main my_main;
  Material[][] material;
  Geometry[][] geometry;

  Geometry[][] pointz;

  private class PointFloat {
    


    private float my_x;
    private float my_y;
    private float my_z;

    public PointFloat(float x, float y, float z) {
      my_x = x;
      my_y = y;
      my_z = z;

    }

    public float getX() {
      return my_x;
    }

    public float getY() {
      return my_y;
    }

    public float getZ() {
      return my_z;
    }

  }

  public Panel(Board board, Main main) {

    my_main = main;

    my_board = board;
    
    
    node = new Node();
    my_camera = my_main.getCamera().clone();

    view = my_main.getRenderManager().createMainView("tetris" , my_camera);
    
    material = new Material[board.height()][board.width()];
    geometry = new Geometry[board.height()][board.width()];
    pointz = new Geometry[board.height()][board.width()];
    for (int i = my_board.height() - 1; 0 <= i; i--) {
      for (int j = 0; j < my_board.width(); j++) {
        final Box b = new Box(Vector3f.ZERO, MULTIPLIER, MULTIPLIER, MULTIPLIER);

        geometry[i][j] = new Geometry("Box" + i + " " + j, b);
        geometry[i][j].setLocalTranslation(((j * MULTIPLIER * 2) - (MULTIPLIER * my_board
            .width())), MULTIPLIER * my_board.height() - (i * MULTIPLIER * 2), MULTIPLIER);

        pointz[i][j] = new Geometry("Box" + i + " " + j + " " + 2, b);
        pointz[i][j].setLocalTranslation(((j * MULTIPLIER * 2) - (MULTIPLIER * my_board
            .width())), MULTIPLIER * my_board.height() - (i * MULTIPLIER * 2), MULTIPLIER);

        TangentBinormalGenerator.generate(b); // for lighting effect
        material[i][j] =
            new Material(my_main.getAssetManager(), "Common/MatDefs/Light/Lighting.j3md");
        material[i][j].setTexture("DiffuseMap",
                                  my_main.getAssetManager().loadTexture("Textures/block.png"));
        material[i][j].setTexture("NormalMap",
                                  my_main.getAssetManager()
                                      .loadTexture("Textures/normal_block.png"));
        material[i][j].setBoolean("UseMaterialColors", true);

        material[i][j].setFloat("Shininess", 10f);


        geometry[i][j].setMaterial(material[i][j]);

        node.attachChild(geometry[i][j]);

      }
    }
    DirectionalLight sun = new DirectionalLight();
    sun.setDirection(new Vector3f(1, 0, -2).normalizeLocal());
    sun.setColor(ColorRGBA.White);

    node.addLight(sun);
    view.attachScene(node);

    
    my_main.getRootNode().attachChild(node);
  }

  public void updateOldPosition() {

    for (int y = 0; y < my_board.height(); y++) {
      for (int x = 0; x < my_board.width(); x++) {
        if (my_board.color(new Point(x, my_board.height() - 1 - y)) == null) {
          if (node.getChild("Box" + y + " " + x) != null) {
            node.detachChild(geometry[y][x]);
          }
        } else {
          if (node.getChild("Box" + y + " " + x) == null) {
            node.attachChild(geometry[y][x]);
          }

          final Color p = my_board.rowAt(my_board.height() - 1 - y)[x];
          if (p != null) {
            ColorRGBA col =
                new ColorRGBA((float) p.getRed() / 255, (float) p.getGreen() / 255,
                              (float) p.getBlue() / 255, (float) p.getAlpha() / 255);
            updateColor(col, new Point(x, my_board.height() - 1 - y));
          }
        }
      }
    }

  }

  public void updateColor(ColorRGBA color, Point point) {
    // if (point.y() < my_board.height()) {

    // my_main.getRootNode()
    // .attachChild(geometry[my_board.height() - 1 - point.y()][point
    // .x()]);

    material[my_board.height() - 1 - point.y()][point.x()].setColor("Specular", color);
    material[my_board.height() - 1 - point.y()][point.x()].setColor("Diffuse", color);

    // }

  }

  public void updatePosition() {

    for (int i = 0; i < my_board.currentPiece().NUMBER_OF_BLOCKS; i++) {
      final Point projected_piece = my_board.projection().absolutePosition(i);
      final Point current_piece = my_board.currentPiece().absolutePosition(i);
      if (current_piece.y() < my_board.height()) {

        final Color p = my_board.currentPiece().color();
        // updateColor(new ColorRGBA((float) p.getRed() / 255, (float)
        // p.getGreen() / 255,
        // (float) p.getBlue() / 255, 0.5f), projected_piece);
        updateColor(new ColorRGBA((float) p.getRed() / 255, (float) p.getGreen() / 255,
                                  (float) p.getBlue() / 255, (float) p.getAlpha() / 255),
                    current_piece);
      }
    }
  }

  public void rotate(float f, float g, float i) {

    for (int y = 0; y < my_board.height(); y++) {
      for (int x = 0; x < my_board.width(); x++) {

        // Quaternion rot = new Quaternion();
        // rot.fromAngleAxis(90, Vector3f.UNIT_Y);
        // geometry[y][x].setLocalRotation(rot);

        // System.out.println(Math.sin((Math.PI * 2) / x));
        final float x_position = (float) (Math.sin(Math.PI * 2) * x);
        final float z_position = (float) (Math.cos(Math.PI * 2) * x);

        // System.out.println(geometry[y][x].getLocalTranslation());

        // if(geometry[y][x].getLocalTranslation().getX() <=
        // pointz[y][x].getZ()) {
        // geometry[y][x].move( ((x * MULTIPLIER * 2) - (MULTIPLIER *
        // my_board.width())), 0, 0 );
        // }

        // geometry[y][x].move(pointz[y][x].getX() * x_position -
        // (float)geometry[y][x].getLocalTranslation().getX() * x_position, 0,
        // pointz[y][x].getX() * x_position -
        // (float)geometry[y][x].getLocalTranslation().getX() * x_position);
        // my_point.getX() * x_angle - the_x * x_angle;
        pointz[y][x].rotate(f, g * 0.1f, i);
        geometry[y][x]
            .setLocalTranslation((pointz[y][x].getLocalTranslation().getX() *
                                  (pointz[y][x].getLocalRotation().getW()) - ((x * MULTIPLIER * 2) - (MULTIPLIER * my_board
                                     .width())) * x_position), geometry[y][x]
                                     .getLocalTranslation().getY(), pointz[y][x]
                                     .getLocalTranslation().getZ() *
                                                                    pointz[y][x]
                                                                        .getLocalRotation()
                                                                        .getY() * z_position);
        geometry[y][x].rotate(f, (-g) * (.05f), i);
      }
    }
  }
}
