
package effects;

import com.jme3.math.Vector3f;
import gui.Panel;
import main.Main;
import tetris.Board;

/**
 * Creates explodable pieces.
 * @author Josef Nosov
 * @version 12/06/2012
 *
 */
public class ExplodeBlocks extends AbstractProjectile {

  /**
   * The amount of blocks in a piece.
   */
  private static final int PIECE_AMOUNT = 4;
  /**
   * The board.
   */
  private final Board my_board;

  /**
   * Creates explodable pieces.
   * @param the_main the main frame.
   * @param the_panel the panel which the exploding pieces will be added.
   * @param the_board the board, required for the width and height.
   */
  public ExplodeBlocks(final Main the_main, final Panel the_panel, 
                       final Board the_board) {
    super(the_main, the_panel, PIECE_AMOUNT * the_board.width());
    my_board = the_board;

  }

  @Override
  protected Vector3f getLocation() {
    return new Vector3f(
        (my_rand.nextInt(my_board.width()) * my_panel.getBlockSize() * 2) - 
        (my_panel.getBlockSize() * my_board.width()), my_panel.getBlockSize() *
         my_board.height(), my_panel.getBlockSize());
  }

  @Override
  protected Vector3f getDirection() {
    return getLocation();
  }

}
