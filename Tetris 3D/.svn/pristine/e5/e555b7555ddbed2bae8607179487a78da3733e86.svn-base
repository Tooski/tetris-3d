
package main;

import gui.Panel;
import gui.SidePanel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;

import java.util.Observable;
import java.util.Observer;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.event.WindowStateListener;
import java.util.Observable;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

import com.jme3.app.SimpleApplication;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.renderer.Camera;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Box;
import com.jme3.system.AppSettings;
import com.jme3.system.JmeCanvasContext;
import com.jme3.util.TangentBinormalGenerator;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.builder.LayerBuilder;
import de.lessvoid.nifty.builder.PanelBuilder;
import de.lessvoid.nifty.builder.ScreenBuilder;
import de.lessvoid.nifty.screen.DefaultScreenController;

import tetris.Board;
import tetris.util.Point;

public class Main extends SimpleApplication implements java.awt.event.ActionListener, Observer {

  private static final int LEVELUP_TIME = 30000;
  private static final int INITIAL_TIME = 1000;
  public final static int FRAME_WIDTH = 640;
  public final static int FRAME_HEIGHT = 480;
  static JmeCanvasContext ctx;

  public static void main(String[] args) {
    /**
     * Ignores information dialog that JMonkey produces example: ignores console
     * output whenever object is added or detached to node. Only prints out
     * severe and necessary lines to console.
     */
    Logger.getLogger("com.jme3").setLevel(Level.SEVERE);
    Logger.getLogger("de.lessvoid").setLevel(Level.SEVERE);
    EventQueue.invokeLater(new Runnable() {
      // class KeyListenerCloseApplication extends KeyAdapter {
      //
      // JFrame my_frame;
      // Main my_main;
      // JmeCanvasContext my_canvas;
      //
      // public KeyListenerCloseApplication(JFrame the_window, Main the_main,
      // JmeCanvasContext canvas) {
      // my_frame = the_window;
      // my_main = the_main;
      // my_canvas = canvas;
      // }
      //
      // public void keyPressed(KeyEvent e) {
      // System.out.println("Test");
      // switch (e.getID()) {
      // case KeyEvent.VK_ESCAPE:
      // my_main.destroy();
      // my_canvas.destroy(true);
      // my_frame.dispose();
      // break;
      // default:
      // break;
      // }
      // }
      // }

      public void run() {
        final AppSettings settings = new AppSettings(true);
        settings.setUseInput(true);
        settings.setWidth(FRAME_WIDTH);
        settings.setHeight(FRAME_HEIGHT);
        window = new JFrame("Tetris 3D");
        my_panel = new JPanel();

        final Main tetris = new Main();
        tetris.setSettings(settings);
        tetris.createCanvas();
        ctx = (JmeCanvasContext) tetris.getContext();
        ctx.setSystemListener(tetris);

        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        final Dimension dim = new Dimension(settings.getWidth(), settings.getHeight());
        ctx.getCanvas().setPreferredSize(dim);
        window.pack();
        window.setVisible(true);

        tetris.startCanvas();

        my_panel.add(ctx.getCanvas(), BorderLayout.WEST);

        window.add(my_panel, BorderLayout.EAST);
        window.pack();

        // window.addKeyListener(new KeyListenerCloseApplication(window, tetris,
        // ctx));
        // my_panel
        // .addKeyListener(new KeyListenerCloseApplication(window, tetris,
        // ctx));
        // ctx.getCanvas()
        // .addKeyListener(new KeyListenerCloseApplication(window, tetris,
        // ctx));
      }
    });
  }

  static JPanel my_panel;
  static JFrame window;

  Panel panel;
  Board my_board;
  SidePanel side_panel;

  private Timer my_timer;

  private enum GameState {
    ACTIVE, GAME_OVER, PAUSED, RESET, START_MENU;
  }

  GameState current_state;

  public Main() {
    Random rand = new Random();
    my_board = new Board(20, 10, rand.nextLong());
    current_state = GameState.ACTIVE;
    my_board.addObserver(this);

  }

  @Override
  public void update(final Observable the_obj, final Object the_arg) {
    // System.err.println(the_obj);

  }

  Node n1 = new Node();
  Node n2 = new Node();


  @Override
  public void simpleInitApp() {
    paused_screen = new PausedScreen(this);
    setDisplayStatView(false);
    setDisplayFps(false);
    viewPort.setBackgroundColor(new ColorRGBA(0.7f, 0.8f, 1f, 1f));
    panel = new Panel(my_board, this);
    flyCam.setEnabled(false);
    inputManager.addMapping("Left", new KeyTrigger(KeyInput.KEY_A),
                            new KeyTrigger(KeyInput.KEY_LEFT));
    inputManager.addMapping("Right", new KeyTrigger(KeyInput.KEY_D),
                            new KeyTrigger(KeyInput.KEY_RIGHT));
    inputManager.addMapping("Up", new KeyTrigger(KeyInput.KEY_W),
                            new KeyTrigger(KeyInput.KEY_UP));
    inputManager.addMapping("Down", new KeyTrigger(KeyInput.KEY_S),
                            new KeyTrigger(KeyInput.KEY_DOWN));
    inputManager.addMapping("Pause", new KeyTrigger(KeyInput.KEY_P));
    inputManager.addMapping("Drop", new KeyTrigger(KeyInput.KEY_Q));
    inputManager.addListener(actionListener, new String[] {"Pause"});
    inputManager.addListener(actionListener, new String[] {"Left", "Right", "Up", "Down",
        "Drop"});
    sp = new SidePanel(my_board);
    my_panel.add(sp);
    window.pack();

    my_timer = new Timer(INITIAL_TIME, this);
    my_timer.start();
    inputManager.addMapping("Drag", new MouseButtonTrigger(0));
    inputManager.addListener(actionListener, "Drag");
    sideGUI();

    window.addComponentListener(new ComponentAdapter() {

      @Override
      public void componentHidden(ComponentEvent arg0) {
        pauseGame(true);
        
      }

      @Override
      public void componentMoved(ComponentEvent arg0) {
        pauseGame(true);
        
      }

      @Override
      public void componentResized(ComponentEvent arg0) {
        ctx.getCanvas().setPreferredSize(new Dimension(window.getWidth(), window.getHeight()));

      }

      @Override
      public void componentShown(ComponentEvent arg0) {
        // TODO Auto-generated method stub
        
      }
      
    });
    
    window.addWindowStateListener(new WindowStateListener () {

      @Override
      public void windowStateChanged(WindowEvent arg0) {
        pauseGame(true);
        ctx.getCanvas().setPreferredSize(new Dimension(window.getWidth(), window.getHeight()));
      }
      
    });
  }

  Geometry[] geometry1;
  Geometry[] geometry2;
  
  public void newCurrentAndNext(Geometry geom,  Material mat, Point p, Color col) {

      geom.setLocalTranslation((p.x() * MULTIPLIER * 2) - 2.2f,
                                  (MULTIPLIER * p.y() * 2 - 1.5f), 0);
    ColorRGBA c =
        new ColorRGBA((float) col.getRed() / 255, (float) col.getGreen() / 255,
                      (float) col.getBlue() / 255, (float) col.getAlpha() / 255);
    mat.setColor("Specular", c);
    mat.setColor("Diffuse", c);
    
  }
  float MULTIPLIER = 1f;


  public void setupCurrentAndNext(Box b, Geometry[] geom, Material[] mat, int i,
                                   Color col, Node node, Point p) {
    geom[i].setLocalTranslation((p.x() * MULTIPLIER * 2) - 2.2f,
                                (MULTIPLIER * p.y() * 2 - 1.5f), 0);
    TangentBinormalGenerator.generate(b);

    mat[i].setTexture("DiffuseMap", getAssetManager().loadTexture("Textures/block.png"));
    mat[i].setTexture("NormalMap", getAssetManager().loadTexture("Textures/normal_block.png"));

    ColorRGBA c =
        new ColorRGBA((float) col.getRed() / 255, (float) col.getGreen() / 255,
                      (float) col.getBlue() / 255, (float) col.getAlpha() / 255);
    mat[i].setBoolean("UseMaterialColors", true);
    mat[i].setColor("Specular", c);
    mat[i].setColor("Diffuse", c);
    mat[i].setFloat("Shininess", 5f);
    geom[i].setMaterial(mat[i]);
    node.attachChild(geom[i]);
  }
  Material[] material1;
  Material[] material2;
  public void sideGUI() {
    

    
    geometry1 = new Geometry[my_board.currentPiece().NUMBER_OF_BLOCKS];
    geometry2 = new Geometry[my_board.currentPiece().NUMBER_OF_BLOCKS];

    
    final Camera current_cam = cam.clone();
    current_cam.setViewPort(0.85f, 1f, 0.75f, 0.90f);
    
    final Camera next_cam = cam.clone();
    next_cam.setViewPort(0.85f, 1f, 0.50f, 0.65f);
    final ViewPort view1 = renderManager.createMainView("current_piece", current_cam);
    final ViewPort view2 = renderManager.createMainView("next_piece", next_cam);

    material1 = new Material[my_board.currentPiece().NUMBER_OF_BLOCKS];
    material2 = new Material[my_board.currentPiece().NUMBER_OF_BLOCKS];


    for (int i = 0; i < my_board.currentPiece().NUMBER_OF_BLOCKS; i++) {
      final Box b = new Box(Vector3f.ZERO, MULTIPLIER, MULTIPLIER, MULTIPLIER);
      geometry1[i] = new Geometry("Tetris c" + i, b);
      geometry2[i] = new Geometry("Tetris n" + i, b);
      material1[i] = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
      material2[i] = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");


      setupCurrentAndNext(b, geometry1, material1, i,  my_board.currentPiece().color(), n1, my_board
          .currentPiece().blocks()[i]);
      
      setupCurrentAndNext(b, geometry2, material2, i,  my_board.nextPiece().color(), n2, my_board
                          .nextPiece().blocks()[i]);
    }
    DirectionalLight sun = new DirectionalLight();
    sun.setDirection(new Vector3f(1, 0, -2).normalizeLocal());
    sun.setColor(ColorRGBA.White);

    n1.addLight(sun);
    n2.addLight(sun);

    view1.attachScene(n1);
    view2.attachScene(n2);
    
    
    final BitmapText current_piece_text = new BitmapText(getAssetManager().loadFont("Interface/Fonts/Default.fnt"), false);
    
    current_piece_text.setSize(getFont().getCharSet().getRenderedSize());
    current_piece_text.setColor(ColorRGBA.Black); 
    current_piece_text.setText("Current Piece");
    current_piece_text.setLocalTranslation(ctx.getCanvas().getWidth() - current_piece_text.getLineWidth(), ctx.getCanvas().getHeight() - current_piece_text.getLineHeight(), 0);
    getGuiNode().attachChild(current_piece_text);
    
    final BitmapText next_piece_text = new BitmapText(getAssetManager().loadFont("Interface/Fonts/Default.fnt"), false);
    next_piece_text.setSize(getFont().getCharSet().getRenderedSize());
    next_piece_text.setColor(ColorRGBA.Black); 
    next_piece_text.setText("Next Piece");
    next_piece_text.setLocalTranslation(ctx.getCanvas().getWidth() - next_piece_text.getLineWidth(), ctx.getCanvas().getHeight() - current_piece_text.getLineHeight()  - current_piece_text.getLineHeight() - next_piece_text.getLineHeight() - ctx.getCanvas().getHeight() * 0.15f, 0);
    getGuiNode().attachChild(next_piece_text);
    

  }

  SidePanel sp;

  public BitmapFont getFont() {
    return guiFont;
  }

  private void pauseGame(boolean b) {
    if (b) {
      paused_screen.isGamePaused(true);
      current_state = GameState.PAUSED;
    } else {
      paused_screen.isGamePaused(false);
      current_state = GameState.ACTIVE;
    }
  }

  private ActionListener actionListener = new ActionListener() {
    public void onAction(String name, boolean pressed, float tpf) {

      if (name.equals("Drag")) {
        isDragging = pressed;
        mouseCoords = new Vector2f(inputManager.getCursorPosition());
      }

      if (pressed) {
        if (current_state == GameState.ACTIVE) {
          panel.updateOldPosition();
          switch (name) {
            case "Up":
              my_board.rotateClockwise();
              break;
            case "Right":
              my_board.moveRight();
              break;
            case "Left":
              my_board.moveLeft();
              break;
            case "Down":
              my_board.moveDown();
              break;
            case "Drop":
              my_board.drop();
              break;
            default:
              break;
          }
        }
        switch (name) {
          case "Pause":
            if (current_state == GameState.ACTIVE) {
              pauseGame(true);
            } else if (current_state == GameState.PAUSED) {
              pauseGame(false);
              paused_screen.isGamePaused(false);

            }
            break;

        }

      }
    }
  };

  private AnalogListener analogListener = new AnalogListener() {
    public void onAnalog(final String name, float value, final float tpf) {
    }
  };

  int speed;

  boolean updated, yes;
  boolean isDragging = false;
  Vector2f mouseCoords = Vector2f.ZERO;
  boolean isUpdated;

  @Override
  public void simpleUpdate(final float tpf) {
    if (current_state == GameState.ACTIVE) {

    for (int i = 0; i < 4; i++) {
      if(my_board.lastBlocksPlaced() > 0) {
        newCurrentAndNext(geometry1[i],  material1[i], my_board.currentPiece().blocks()[i], my_board.currentPiece().color());
        newCurrentAndNext(geometry2[i],  material2[i], my_board.nextPiece().blocks()[i], my_board.nextPiece().color());

      }
        geometry1[i].rotate(0.0f, tpf/3, 0.0f);
        geometry2[i].rotate(0.0f, tpf/3, 0.0f);
      
    }
    n1.updateGeometricState();
    n1.updateLogicalState(tpf);
    n2.updateGeometricState();
    n2.updateLogicalState(tpf);

      if (my_board.changed()) {
        if (my_board.lastLinesRemoved() > 0 && !isUpdated) {
          if (my_board.lastLinesRemoved() == 4) {
            sp.addPoints(my_board.lastLinesRemoved() * 4);
          } else {
            sp.addPoints(my_board.lastLinesRemoved());
          }
          isUpdated = false;
        }
        panel.updateOldPosition();
        panel.updatePosition();
      }
      if (my_board.isFull()) {
        paused_screen.isGameOver(true);

        current_state = GameState.GAME_OVER;

      }

      if (isDragging) {

        final Vector2f curPosition = inputManager.getCursorPosition();
        final Vector2f movement = curPosition.subtract(mouseCoords);

        panel.rotate(0, movement.x, 0);

        mouseCoords = inputManager.getCursorPosition().clone();
      }
    }

  }

  private static int add_number;
  PausedScreen paused_screen;

  @Override
  public void actionPerformed(final ActionEvent e) {
    if (current_state == GameState.ACTIVE) {
      isUpdated = true;
      add_number += my_timer.getDelay();
      if (add_number >= LEVELUP_TIME) {
        if (my_timer.getDelay() >= 300) {
          my_timer.setDelay((int) (my_timer.getDelay() * 0.95));
        }
        sp.levelUp();
        add_number = 0;
      }
      my_board.moveDown();
      sp.repaint();

    }
  }

}
